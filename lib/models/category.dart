import 'package:flutter/material.dart';
import 'package:meals_app/services/database_helper.dart';

class Category {
  int id;
  String name;
  String imageUrl;

  Category({
    this.id,
    @required this.name,
    @required this.imageUrl,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseHelper.COLUMN_NAME: name,
      DatabaseHelper.COLUMN_IMAGEURL: imageUrl,
      // DatabaseHelper.COLUMN_ISFAVORITE: isFavorite ? 1 : 0,
    };

    if (id != null) {
      map[DatabaseHelper.COLUMN_ID] = id;
    }
    return map;
  }

  Category.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseHelper.COLUMN_ID];
    name = map[DatabaseHelper.COLUMN_NAME];
    imageUrl = map[DatabaseHelper.COLUMN_IMAGEURL];
    // isFavorite = map[DatabaseHelper.COLUMN_ISFAVORITE] == 1;
  }
}
