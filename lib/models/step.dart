import 'package:flutter/material.dart';
import 'package:meals_app/services/database_helper.dart';

class MyStep {
  int id;
  String name;
  int meal;

  MyStep({
    this.id,
    @required this.name,
    this.meal,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseHelper.COLUMN_STEP_NAME: name,
      DatabaseHelper.FK_STEP_MEAL: meal
    };

    if (id != null) {
      map[DatabaseHelper.COLUMN_STEP_ID] = id;
    }
    return map;
  }

  MyStep.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseHelper.COLUMN_STEP_ID];
    name = map[DatabaseHelper.COLUMN_STEP_NAME];
    meal = map[DatabaseHelper.FK_STEP_MEAL];
  }
}
