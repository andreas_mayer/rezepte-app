import 'package:flutter/material.dart';
import 'package:meals_app/services/database_helper.dart';

class Ingredient {
  int id;
  String name;
  int meal;

  Ingredient({
    this.id,
    @required this.name,
    this.meal,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseHelper.COLUMN_INGREDIENT_NAME: name,
      DatabaseHelper.FK_INGREDIENT_MEAL: meal,
    };

    if (id != null) {
      map[DatabaseHelper.COLUMN_INGREDIENT_ID] = id;
    }
    return map;
  }

  Ingredient.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseHelper.COLUMN_INGREDIENT_ID];
    name = map[DatabaseHelper.COLUMN_INGREDIENT_NAME];
    meal = map[DatabaseHelper.FK_INGREDIENT_MEAL];
  }
}
