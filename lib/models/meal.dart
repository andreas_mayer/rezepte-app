import 'package:flutter/material.dart';
import 'package:meals_app/services/database_helper.dart';

class Meal {
  int id;
  String category;
  String title;
  String imageUrl;
  // List<String> ingredients;
  // List<String> steps;
  int duration;
  String complexity;
  bool isGlutenFree;
  bool isLactoseFree;
  bool isVegan;
  bool isVegetarian;
  int isFavorite;

  Meal({
    this.id,
    this.category,
    @required this.title,
    this.imageUrl,
    // this.ingredients,
    // this.steps,
    @required this.duration,
    @required this.complexity,
    this.isGlutenFree,
    this.isLactoseFree,
    this.isVegan,
    this.isVegetarian,
    this.isFavorite,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseHelper.COLUMN_CATEGORY: category,
      DatabaseHelper.COLUMN_TITLE: title,
      DatabaseHelper.COLUMN_IMAGEURL: imageUrl,
      // DatabaseHelper.COLUMN_INGREDIENTS: ingredients,
      // DatabaseHelper.COLUMN_STEPS: steps,
      DatabaseHelper.COLUMN_DURATION: duration,
      DatabaseHelper.COLUMN_COMPLEXITY: complexity,
      // DatabaseHelper.COLUMN_ISGLUTENFREE: isGlutenFree ? 1 : 0,
      // DatabaseHelper.COLUMN_ISLACTOSEFREE: isLactoseFree ? 1 : 0,
      // DatabaseHelper.COLUMN_ISVEGAN: isVegan ? 1 : 0,
      // DatabaseHelper.COLUMN_ISVEGETARIAN: isVegetarian ? 1 : 0,
      // DatabaseHelper.COLUMN_ISFAVORITE: isFavorite ? 1 : 0,
      // DatabaseHelper.FK_MEAL_CATEGORY: category
      DatabaseHelper.COLUMN_ISFAVORITE: isFavorite,
    };

    if (id != null) {
      map[DatabaseHelper.COLUMN_MEAL_ID] = id;
    }
    return map;
  }

  Meal.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseHelper.COLUMN_MEAL_ID];
    category = map[DatabaseHelper.COLUMN_CATEGORY];
    title = map[DatabaseHelper.COLUMN_TITLE];
    imageUrl = map[DatabaseHelper.COLUMN_IMAGEURL];
    // ingredients = map[DatabaseHelper.COLUMN_INGREDIENTS];
    // steps = map[DatabaseHelper.COLUMN_STEPS];
    duration = map[DatabaseHelper.COLUMN_DURATION];
    complexity = map[DatabaseHelper.COLUMN_COMPLEXITY];
    // isGlutenFree = map[DatabaseHelper.COLUMN_ISGLUTENFREE] == 1;
    // isLactoseFree = map[DatabaseHelper.COLUMN_ISLACTOSEFREE] == 1;
    // isVegan = map[DatabaseHelper.COLUMN_ISVEGAN] == 1;
    // isVegetarian = map[DatabaseHelper.COLUMN_ISVEGETARIAN] == 1;
    // isFavorite = map[DatabaseHelper.COLUMN_ISFAVORITE] == 1;
    // category = map[DatabaseHelper.FK_MEAL_CATEGORY];
    isFavorite = map[DatabaseHelper.COLUMN_ISFAVORITE];
  }
}
