import 'package:meals_app/models/step.dart';
import 'package:meals_app/services/database_helper.dart';
import 'package:flutter/material.dart';

class StepProvider with ChangeNotifier {
  int _stepId;
  String _name;
  int _meal;

  //****************************************************************************/
  // GETTER
  //****************************************************************************/

  String get name => _name;
  int get meal => _meal;

  int get stepId => _stepId;

  Future<List<MyStep>> get steps => DatabaseHelper.db.getSteps();

  //****************************************************************************/
  // SETTER
  //****************************************************************************/

  set changeName(String name) {
    _name = name;
    notifyListeners();
  }

  set changeMeal(int meal) {
    _meal = meal;
    notifyListeners();
  }

  //****************************************************************************/
  // FUNCTIONS
  //***************************************************************************/

  saveStep() async {
    if (_stepId == null) {
      var newStep = MyStep(
        name: _name,
        meal: _meal,
      );
      DatabaseHelper.db.insertStep(newStep);
      notifyListeners();
    } else {
      // Edit
      var updatedStep = MyStep(
        id: _stepId,
        name: _name,
        meal: _meal,
      );
      DatabaseHelper.db.updateStep(updatedStep);
      notifyListeners();
    }
  }

  removeStep(int stepId) {
    DatabaseHelper.db.deleteStep(stepId);
    notifyListeners();
  }
}
