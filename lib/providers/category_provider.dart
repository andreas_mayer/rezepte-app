import 'package:meals_app/models/category.dart';
import 'package:meals_app/services/database_helper.dart';
import 'package:flutter/material.dart';

class CategoryProvider with ChangeNotifier {
  int _categoryId;
  String _name;
  String _imageUrl;

  //****************************************************************************/
  // GETTER
  //****************************************************************************/

  String get name => _name;
  String get imageUrl => _imageUrl;

  int get categoryId => _categoryId;

  Future<List<Category>> get categories => DatabaseHelper.db.getCategories();

  //****************************************************************************/
  // SETTER
  //****************************************************************************/

  set changeName(String name) {
    _name = name;
    notifyListeners();
  }

  set changeImageUrl(String imageUrl) {
    _imageUrl = imageUrl;
    notifyListeners();
  }

  //****************************************************************************/
  // FUNCTIONS
  //***************************************************************************/

  loadAll(Category category) {
    if (category != null) {
      _categoryId = category.id;
      _name = category.name;
      _imageUrl = category.imageUrl;
    } else {
      _categoryId = null;
      _name = null;
      _imageUrl = null;
    }
  }

  saveCategory() async {
    if (_categoryId == null) {
      var newCategory = Category(
        name: _name,
        imageUrl: _imageUrl,
      );
      DatabaseHelper.db.insert(newCategory);
      notifyListeners();
    } else {
      // Edit
      var updatedCategory = Category(
        id: _categoryId,
        name: _name,
        imageUrl: _imageUrl,
      );
      DatabaseHelper.db.update(updatedCategory);
      notifyListeners();
    }
  }

  removeCategory(int categoryId) {
    DatabaseHelper.db.delete(categoryId);
    notifyListeners();
  }
}
