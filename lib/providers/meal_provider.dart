import 'package:flutter/material.dart';
import 'package:meals_app/models/meal.dart';
import 'package:meals_app/services/database_helper.dart';

class MealProvider with ChangeNotifier {
  int _id;
  String _category;
  String _title;
  String _imageUrl;
  List<String> _ingredients;
  List<String> _steps;
  int _duration;
  String _complexity;
  bool _isGlutenFree;
  bool _isLactoseFree;
  bool _isVegan;
  bool _isVegetarian;
  int _isFavorite;

  //****************************************************************************/
  // GETTER
  //****************************************************************************/
  String get category => _category;
  String get title => _title;
  String get imageUrl => _imageUrl;
  List<String> get ingredients => _ingredients;
  List<String> get steps => _steps;
  int get duration => _duration;
  String get complexity => _complexity;
  bool get isGlutenFree => _isGlutenFree;
  bool get isLactoseFree => _isLactoseFree;
  bool get isVegan => _isVegan;
  bool get isVegetarian => _isVegetarian;
  int get isFavorite => _isFavorite;

  // GET MEALS FROM DB
  Future<List<Meal>> get meals => DatabaseHelper.db.getMeals();

  // GET MEALS BY Id
  // Future<List<Meal>> get mealById => DatabaseHelper.db.getMealById(_id);

  // GET FAVORITE MEALS
  // ToDo

  //****************************************************************************/
  // SETTERS
  //****************************************************************************/
  set changeCategory(String category) {
    _category = category;
    notifyListeners();
  }

  set changeTitle(String title) {
    _title = title;
    notifyListeners();
  }

  set changeImageUrl(String imageUrl) {
    _imageUrl = imageUrl;
    notifyListeners();
  }

  set changeDuration(int duration) {
    _duration = duration;
    notifyListeners();
  }

  set changeComplexity(String complexity) {
    _complexity = complexity;
    notifyListeners();
  }

  set changeIngredients(List ingredients) {
    _ingredients = ingredients;
    notifyListeners();
  }

  set changeSteps(List steps) {
    _steps = steps;
    notifyListeners();
  }

  set changeIsFavorite(int isFavorite) {
    _isFavorite = isFavorite;
  }

  //****************************************************************************/
  // FUNCTIONS
  //****************************************************************************/

  loadAll(Meal meal) {
    if (meal != null) {
      _id = meal.id;
      _title = meal.title;
      _imageUrl = meal.imageUrl;
      _duration = meal.duration;
      _complexity = meal.complexity;
      _category = meal.category;
      _isFavorite = meal.isFavorite;
      // _ingredients = meal.ingredients;
      // _steps = meal.steps;
    } else {
      _id = null;
      _title = null;
      _imageUrl = null;
      _duration = null;
      _complexity = null;
      _category = null;
      _isFavorite = null;
      // _ingredients = null;
      // _steps = null;
    }
  }

  // mealById(int id) async {
  //   var currentMeal = Meal(
  //     _id = id;
  //     _title = meal.title;
  //     _imageUrl = meal.imageUrl;
  //     _duration = meal.duration;
  //     _complexity = meal.complexity;
  //     _category = meal.category;
  //     // ingredients: _ingredients,
  //     // steps: _steps,
  //   );
  //   DatabaseHelper.db.getMealById(currentMeal);
  //   notifyListeners();
  // }

  saveMeal() async {
    if (_id == null) {
      // ADD NEW MEAL
      print(_id);
      print(_title);
      print(_category);
      print(_complexity);
      var newMeal = Meal(
        title: _title,
        imageUrl: _imageUrl,
        duration: _duration,
        complexity: _complexity,
        category: _category,
        isFavorite: _isFavorite,
        // ingredients: _ingredients,
        // steps: _steps,
      );
      DatabaseHelper.db.insertMeal(newMeal);
      notifyListeners();
    } else {
      // UPDATE CURRENT MEAL
      print(_id);
      print(_title);
      print(_category);
      print(_complexity);
      var updatedMeal = Meal(
        id: _id,
        title: _title,
        imageUrl: _imageUrl,
        duration: _duration,
        complexity: _complexity,
        category: _category,
        isFavorite: _isFavorite,
        // ingredients: _ingredients,
        // steps: _steps,
      );
      DatabaseHelper.db.updateMeal(updatedMeal);
      notifyListeners();
    }
  }

  removeMeal(int id) {
    DatabaseHelper.db.deleteMeal(id);
    notifyListeners();
  }
}
