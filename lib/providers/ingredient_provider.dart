import 'package:meals_app/models/ingredient.dart';
import 'package:meals_app/services/database_helper.dart';
import 'package:flutter/material.dart';

class IngredientProvider with ChangeNotifier {
  int _ingredientId;
  String _name;
  int _meal;

  //****************************************************************************/
  // GETTER
  //****************************************************************************/

  String get name => _name;
  int get meal => _meal;

  int get ingredientId => _ingredientId;

  Future<List<Ingredient>> get ingredients =>
      DatabaseHelper.db.getIngredients();

  //****************************************************************************/
  // SETTER
  //****************************************************************************/

  set changeName(String name) {
    _name = name;
    notifyListeners();
  }

  set changeMeal(int meal) {
    _meal = meal;
    notifyListeners();
  }

  //****************************************************************************/
  // FUNCTIONS
  //***************************************************************************/

  saveIngredient() async {
    if (_ingredientId == null) {
      var newIngredient = Ingredient(
        name: _name,
        meal: _meal,
      );
      DatabaseHelper.db.insertIngredient(newIngredient);
      notifyListeners();
    } else {
      // Edit
      var updatedIngredient = Ingredient(
        id: _ingredientId,
        name: _name,
        meal: _meal,
      );
      DatabaseHelper.db.updateIngredient(updatedIngredient);
      notifyListeners();
    }
    print(_name);
  }

  removeIngredient(int ingredientId) {
    DatabaseHelper.db.deleteIngredient(ingredientId);
    notifyListeners();
  }
}
