import 'package:flutter/material.dart';
import 'package:meals_app/providers/category_provider.dart';
import 'package:meals_app/providers/ingredient_provider.dart';
import 'package:meals_app/providers/meal_provider.dart';
import 'package:meals_app/providers/step_provider.dart';
import 'package:meals_app/screens/all_meals_screen.dart';
import 'package:meals_app/screens/edit_categorie_screen.dart';
import 'package:meals_app/screens/edit_meal_screen.dart';
import 'package:meals_app/screens/user_categories_screen.dart';
import 'package:meals_app/screens/user_meals_screen.dart';
import './screens/filters_screen.dart';
import './screens/tabs_screen.dart';
import './screens/meal_detail_screen.dart';
import './screens/category_meals_screen.dart';
import './screens/categories_screen.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // Map<String, bool> _filters = {
  //   'gluten': false,
  //   'lactose': false,
  //   'vegan': false,
  //   'vegetarian': false,
  // };

  // List<Meal> _favoriteMeals = [];
  // List<Meal> _availableMeals = [];

  // void _setFilters(Map<String, bool> filterData) {
  //   setState(() {
  //     _filters = filterData;

  //     _availableMeals = DUMMY_MEALS.where((meal) {
  //       if (_filters['gluten'] == true && !meal.isGlutenFree) {
  //         return false;
  //       }
  //       if (_filters['lactose'] == true && !meal.isLactoseFree) {
  //         return false;
  //       }
  //       if (_filters['vegan'] == true && !meal.isVegan) {
  //         return false;
  //       }
  //       if (_filters['vegetarian'] == true && !meal.isVegetarian) {
  //         return false;
  //       }
  //       return true;
  //     }).toList();
  //   });
  // }

  // void _toggleFavorite(String mealId) {
  //   final existingIndex =
  //       _favoriteMeals.indexWhere((meal) => meal.id == mealId);
  //   if (existingIndex >= 0) {
  //     setState(() {
  //       _favoriteMeals.removeAt(existingIndex);
  //     });
  //   } else {
  //     setState(() {
  //       _favoriteMeals.add(
  //         DUMMY_MEALS.firstWhere((meal) => meal.id == mealId),
  //       );
  //     });
  //   }
  // }

  // bool _isMealFavorite(String id) {
  //   return _favoriteMeals.any((meal) => meal.id == id);
  // }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => CategoryProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => MealProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => IngredientProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => StepProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Rezepte',
        theme: ThemeData(
          primaryColor: Color.fromRGBO(0, 104, 139, 1),
          accentColor: Colors.deepOrange[600],
          // primarySwatch: Colors.pink,
          // accentColor: Colors.amber,
          canvasColor: Color.fromRGBO(255, 254, 229, 1),
          fontFamily: 'Raleway',
          textTheme: ThemeData.light().textTheme.copyWith(
                bodyText1: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
                bodyText2: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
                headline6: TextStyle(
                  fontSize: 24,
                  fontFamily: 'RobotoCondensed',
                  fontWeight: FontWeight.bold,
                ),
              ),
        ),
        //home: CategoriesScreen(),
        initialRoute: '/',
        routes: {
          '/': (ctx) => TabsScreen(),
          CategoryMealsScreen.routeName: (ctx) => CategoryMealsScreen(),
          // MealDetailScreen.routeName: (ctx) =>
          //     MealDetailScreen(_toggleFavorite, _isMealFavorite),
          MealDetailScreen.routeName: (ctx) => MealDetailScreen(),
          FiltersScreen.routeName: (ctx) => FiltersScreen(),
          EditMealScreen.routeName: (ctx) => EditMealScreen(),
          EditCategoryScreen.routeName: (ctx) => EditCategoryScreen(),
          UserCategoriesScreen.routeName: (ctx) => UserCategoriesScreen(),
          UserMealsScreen.routeName: (ctx) => UserMealsScreen(),
          AllMealsScreen.routeName: (ctx) => AllMealsScreen(),
        },
        //fallback Schutzmechanismus (wenn eine Seite nicht existiert, dann zu CategoriesScreen)
        onUnknownRoute: (settings) {
          return MaterialPageRoute(
            builder: (ctx) => CategoriesScreen(),
          );
        },
      ),
    );
  }
}
