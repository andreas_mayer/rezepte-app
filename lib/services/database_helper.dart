import 'package:meals_app/models/category.dart';
import 'package:meals_app/models/ingredient.dart';
import 'package:meals_app/models/meal.dart';
import 'package:meals_app/models/step.dart';
import 'package:path/path.dart';

import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static const TABLE_CATEGORY = 'categories';
  static const TABLE_MEAL = 'meals';
  static const TABLE_INGREDIENT = 'ingredients';
  static const TABLE_STEP = 'steps';

  // Table Category
  static const COLUMN_ID = 'id';
  static const COLUMN_NAME = 'name';
  static const COLUMN_IMAGEURL = 'imageUrl';

  // Table Meal
  static const COLUMN_MEAL_ID = 'id';
  static const COLUMN_CATEGORY = 'category';
  static const COLUMN_TITLE = 'titel';
  static const COLUMN_MEAL_IMAGEURL = 'meal_imageurl';
  // static const COLUMN_INGREDIENTS = 'ingredients';
  // static const COLUMN_STEPS = 'steps';
  static const COLUMN_DURATION = 'duration';
  static const COLUMN_COMPLEXITY = 'complexity';
  static const COLUMN_ISGLUTENFREE = 'isGlutenFree';
  static const COLUMN_ISLACTOSEFREE = 'isLactoseFree';
  static const COLUMN_ISVEGAN = 'isVegan';
  static const COLUMN_ISVEGETARIAN = 'isVegetarian';
  static const COLUMN_ISFAVORITE = 'isFavorite';
  static const FK_MEAL_CATEGORY = 'FK_meal_category';

  // Table Ingredients
  static const COLUMN_INGREDIENT_ID = 'id';
  static const COLUMN_INGREDIENT_NAME = 'ingredientName';
  static const FK_INGREDIENT_MEAL = 'FK_ingredient_meal';

  // Table Steps
  static const COLUMN_STEP_ID = 'id';
  static const COLUMN_STEP_NAME = 'stepName';
  static const FK_STEP_MEAL = 'FK_step_meal';

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper db = DatabaseHelper._privateConstructor();

  Database _database;

  Future<Database> get database async {
    print('database getter called');
    if (_database != null) {
      return _database;
    }

    _database = await createDatabase();

    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();

    return await openDatabase(
      join(dbPath, 'recipeDB.db'),
      version: 1,
      onCreate: (Database database, int version) async {
        print('Creating two tables');

        await database.execute(
          "CREATE TABLE $TABLE_CATEGORY ("
          "$COLUMN_ID INTEGER PRIMARY KEY,"
          "$COLUMN_NAME STRING,"
          "$COLUMN_IMAGEURL STRING"
          ")",
        );
        await database.execute(
          "CREATE TABLE $TABLE_MEAL ("
          "$COLUMN_MEAL_ID INTEGER PRIMARY KEY,"
          "$COLUMN_CATEGORY STRING,"
          "$COLUMN_TITLE STRING,"
          "$COLUMN_IMAGEURL STRING,"
          // "$COLUMN_INGREDIENTS TEXT,"
          // "$COLUMN_STEPS TEXT,"
          "$COLUMN_DURATION INTEGER,"
          "$COLUMN_COMPLEXITY STRING,"
          // "$COLUMN_ISGLUTENFREE INTEGER,"
          // "$COLUMN_ISLACTOSEFREE INTEGER,"
          // "$COLUMN_ISVEGAN INTEGER,"
          // "$COLUMN_ISVEGETARIAN INTEGER,"
          "$COLUMN_ISFAVORITE INTEGER"
          ")",
        );
        await database.execute(
          "CREATE TABLE $TABLE_INGREDIENT ("
          "$COLUMN_INGREDIENT_ID INTEGER PRIMARY KEY,"
          "$COLUMN_INGREDIENT_NAME STRING,"
          "$FK_INGREDIENT_MEAL INTEGER,"
          "FOREIGN KEY ($FK_INGREDIENT_MEAL) REFERENCES $TABLE_MEAL ($COLUMN_MEAL_ID)"
          ")",
        );
        await database.execute(
          "CREATE TABLE $TABLE_STEP ("
          "$COLUMN_STEP_ID INTEGER PRIMARY KEY,"
          "$COLUMN_STEP_NAME STRING,"
          "$FK_STEP_MEAL INTEGER,"
          "FOREIGN KEY ($FK_STEP_MEAL) REFERENCES $TABLE_MEAL ($COLUMN_MEAL_ID)"
          ")",
        );
      },
    );
  }

  // CATEGORIES
  Future<List<Category>> getCategories() async {
    final db = await database;

    List<Map<String, dynamic>> allRows = await db.query('categories');
    List<Category> categoryList =
        allRows.map((category) => Category.fromMap(category)).toList();
    return categoryList;
  }

  Future<Category> insert(Category category) async {
    final db = await database;
    // Definiert Id für die Kategorie nach dem Einfügen
    category.id = await db.insert(TABLE_CATEGORY, category.toMap());
    print(category.id);
    return category;
  }

  Future<int> delete(int id) async {
    final db = await database;

    return await db.delete(
      TABLE_CATEGORY,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<int> update(Category category) async {
    final db = await database;

    return await db.update(
      TABLE_CATEGORY,
      category.toMap(),
      where: "id = ?",
      whereArgs: [category.id],
    );
  }

  // MEALS
  Future<List<Meal>> getMeals() async {
    final db = await database;

    List<Map<String, dynamic>> allRows = await db.query('meals');
    List<Meal> mealList = allRows.map((meal) => Meal.fromMap(meal)).toList();
    return mealList;
  }

  Future<Meal> insertMeal(Meal meal) async {
    final db = await database;
    // Definiert Id für die Kategorie nach dem Einfügen
    meal.id = await db.insert(TABLE_MEAL, meal.toMap());
    print(meal.id);
    return meal;
  }

  Future<int> deleteMeal(int id) async {
    final db = await database;

    return await db.delete(
      TABLE_MEAL,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<int> updateMeal(Meal meal) async {
    final db = await database;
    print('DB Update Meal');
    print(meal.id);
    print(meal.category);
    print('Favoritestatus:');
    print(meal.isFavorite);
    return await db.update(
      TABLE_MEAL,
      meal.toMap(),
      where: "id = ?",
      whereArgs: [meal.id],
    );
  }

  // Ingredients
  Future<List<Ingredient>> getIngredients() async {
    final db = await database;

    List<Map<String, dynamic>> allRows = await db.query('ingredients');
    List<Ingredient> ingredientList =
        allRows.map((ingredient) => Ingredient.fromMap(ingredient)).toList();
    return ingredientList;
  }

  Future<Ingredient> insertIngredient(Ingredient ingredient) async {
    final db = await database;
    // Definiert Id für die Kategorie nach dem Einfügen
    ingredient.id = await db.insert(TABLE_INGREDIENT, ingredient.toMap());
    return ingredient;
  }

  Future<int> deleteIngredient(int id) async {
    final db = await database;

    return await db.delete(
      TABLE_INGREDIENT,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<int> updateIngredient(Ingredient ingredient) async {
    final db = await database;

    return await db.update(
      TABLE_INGREDIENT,
      ingredient.toMap(),
      where: "id = ?",
      whereArgs: [ingredient.id],
    );
  }

  // Steps
  Future<List<MyStep>> getSteps() async {
    final db = await database;

    List<Map<String, dynamic>> allRows = await db.query('steps');
    List<MyStep> stepList =
        allRows.map((step) => MyStep.fromMap(step)).toList();
    return stepList;
  }

  Future<MyStep> insertStep(MyStep step) async {
    final db = await database;
    // Definiert Id für die Kategorie nach dem Einfügen
    step.id = await db.insert(TABLE_STEP, step.toMap());
    return step;
  }

  Future<int> deleteStep(int id) async {
    final db = await database;

    return await db.delete(
      TABLE_STEP,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<int> updateStep(MyStep step) async {
    final db = await database;

    return await db.update(
      TABLE_STEP,
      step.toMap(),
      where: "id = ?",
      whereArgs: [step.id],
    );
  }
}
