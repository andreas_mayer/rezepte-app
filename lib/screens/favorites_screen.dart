import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:meals_app/providers/meal_provider.dart';
import '../widgets/meal_item.dart';
import 'package:provider/provider.dart';

class FavoritesScreen extends StatelessWidget {
  static const routeName = '/favorites';

  @override
  Widget build(BuildContext context) {
    final mealList = Provider.of<MealProvider>(context).meals;
    final altImage = 'assets/images/placeholder_image.jpg';
    final altComplexitiy = 'finde es heraus ;)';
    Uint8List decodedBytes;

    return FutureBuilder(
      future: mealList,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.data.length != null) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (ctx, index) {
              if (snapshot.data[index].isFavorite == null) {
                return SizedBox();
              } else if (!snapshot.data[index].isFavorite
                  .toString()
                  .contains('1')) {
                return SizedBox();
              } else {
                if (snapshot.data[index].imageUrl != null) {
                  decodedBytes = base64Decode(snapshot.data[index].imageUrl);
                  print('Bild vorhanden: $decodedBytes');
                } else {
                  decodedBytes = null;
                  print('Bild null: $decodedBytes');
                }
                return MealItem(
                  id: snapshot.data[index].id,
                  title: snapshot.data[index].title,
                  decodedBytes: decodedBytes,
                  duration: snapshot.data[index].duration,
                  complexity: snapshot.data[index].complexity ?? altComplexitiy,
                  isFavorite: snapshot.data[index].isFavorite,
                  category: snapshot.data[index].category,

                  // ingredients: snapshot.data[index].ingredients,
                  // steps: snapshot.data[index].steps,
                );
              }
            },
          );
        } else {
          return SizedBox();
        }
      },
    );
  }
}
