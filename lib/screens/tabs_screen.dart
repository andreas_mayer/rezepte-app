import 'package:flutter/material.dart';
import 'package:meals_app/widgets/main_drawer.dart';
import '../screens/favorites_screen.dart';
import '../screens/categories_screen.dart';
import 'all_meals_screen.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Rezepte',
            style: TextStyle(
              fontFamily: 'RobotoCondensed',
              fontSize: 30,
              fontWeight: FontWeight.w700,
              color: Theme.of(context).accentColor,
            ),
          ),
          bottom: TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.category),
                text: 'Kategorien',
              ),
              Tab(
                icon: Icon(Icons.star),
                text: 'Favoriten',
              ),
              Tab(
                icon: Icon(Icons.restaurant_menu),
                text: 'Alle Rezepte',
              ),
            ],
          ),
        ),
        drawer: MainDrawer(),

        //Auswahl Element in der Liste über Index und Zugriff auf Element in Map über Key
        body: TabBarView(
          children: [
            CategoriesScreen(),
            FavoritesScreen(),
            AllMealsScreen(),
          ],
        ),
      ),
    );
  }
}
