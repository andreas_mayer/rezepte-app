import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meals_app/models/category.dart';
import 'package:meals_app/providers/category_provider.dart';
import 'package:provider/provider.dart';

class EditCategoryScreen extends StatefulWidget {
  static const routeName = 'edit-category';

  final Category category;

  EditCategoryScreen({this.category});

  @override
  _EditCategoryScreenState createState() => _EditCategoryScreenState();
}

class _EditCategoryScreenState extends State<EditCategoryScreen> {
  Uint8List _imageFromDB;

  final _formKey1 = GlobalKey<FormState>();
  // final _formKey2 = GlobalKey<FormState>();

  final nameController = TextEditingController();
  final imageUrlController = TextEditingController();

  @override
  void dispose() {
    nameController.dispose();
    imageUrlController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    final categoryProvider =
        Provider.of<CategoryProvider>(context, listen: false);

    if (widget.category != null) {
      nameController.text = widget.category.name;
      imageUrlController.text = widget.category.imageUrl;

      if (imageUrlController.text != null && imageUrlController.text != '') {
        _imageFromDB = base64Decode(imageUrlController.text);
      }

      // Update from Backend
      categoryProvider.loadAll(widget.category);
    } else {
      //Add new Category
      categoryProvider.loadAll(null);
    }
    super.initState();
  }

  File _imageFile;
  final picker = ImagePicker();

  // Image von der Kamera
  Future pickImageFromCamera() async {
    final categoryProvider =
        Provider.of<CategoryProvider>(context, listen: false);
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 20);

    final bytes = await pickedFile.readAsBytes();
    String img64 = base64Encode(bytes);

    setState(() {
      _imageFromDB = null;
      categoryProvider.changeImageUrl = img64;
      _imageFile = File(pickedFile.path);
      imageUrlController.clear();
    });
  }

  // Image aus der Gallery
  Future pickImageFromGallery() async {
    final categoryProvider =
        Provider.of<CategoryProvider>(context, listen: false);
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 50);

    final bytes = await pickedFile.readAsBytes();
    String img64 = base64Encode(bytes);
    setState(() {
      _imageFromDB = null;
      categoryProvider.changeImageUrl = img64;
      _imageFile = File(pickedFile.path);
      imageUrlController.clear();
    });
  }

  // Bild aus Vorschau löschen
  void _clear() {
    final categoryProvider =
        Provider.of<CategoryProvider>(context, listen: false);
    setState(() {
      _imageFromDB = null;
      categoryProvider.changeImageUrl = null;
      // imageUrlController.text = null;
      imageUrlController.clear();
      _imageFile = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    final categoryProvider = Provider.of<CategoryProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Kategorie bearbeiten',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () async {
                final isValid = _formKey1.currentState.validate();
                if (!isValid) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: Colors.red,
                      content: const Text(
                        'Überprüfe bitte nochmal deine Eingabe',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  );
                  return;
                }
                _formKey1.currentState.save();

                categoryProvider.changeName = nameController.text;

                // if (imageUrlController.text == '') {
                //   if (_imageFile != null) {
                //     print('Image vorhanden');
                //     await uploadImageToFirebase(context);
                //   }
                //   print('Kein Image');
                // }

                categoryProvider.saveCategory();
                Navigator.of(context).pop();
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 100,
              width: double.infinity,
              child: Form(
                key: _formKey1,
                child: TextFormField(
                  decoration: InputDecoration(labelText: 'Name'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Bitte einen Namen für die neue Kategorie eingeben';
                    }
                    return null;
                  },
                  onFieldSubmitted: (value) =>
                      categoryProvider.changeName = value,
                  controller: nameController,
                  textInputAction: TextInputAction.next,
                ),
              ),
            ),
            // Container(
            //   decoration: BoxDecoration(
            //     color: Colors.white,
            //     border: Border.all(color: Colors.grey),
            //     borderRadius: BorderRadius.circular(10),
            //   ),
            //   margin: EdgeInsets.all(20),
            //   padding: EdgeInsets.all(10),
            //   height: 100,
            //   width: double.infinity,
            //   child: Form(
            //     key: _formKey2,
            //     child: TextFormField(
            //       decoration: InputDecoration(labelText: 'ImageUrl'),
            //       onChanged: (String value) =>
            //           categoryProvider.changeImageUrl = value,
            //       controller: imageUrlController,
            //       textInputAction: TextInputAction.next,
            //     ),
            //   ),
            // ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FloatingActionButton(
                  heroTag: 'buttonCamera',
                  backgroundColor: Theme.of(context).accentColor,
                  child: Icon(Icons.add_a_photo),
                  onPressed: () => pickImageFromCamera(),
                ),
                SizedBox(
                  width: 30,
                ),
                FloatingActionButton(
                  heroTag: 'buttonAssets',
                  backgroundColor: Theme.of(context).accentColor,
                  child: Icon(Icons.photo_album),
                  onPressed: () => pickImageFromGallery(),
                ),
                SizedBox(
                  width: 30,
                ),
                FloatingActionButton(
                  heroTag: 'buttonClear',
                  backgroundColor: Theme.of(context).accentColor,
                  child: Icon(Icons.delete),
                  onPressed: _clear,
                ),
              ],
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 200,
              width: double.infinity,
              child: Center(
                child: _imageFile != null
                    ? Image.file(
                        _imageFile,
                        height: double.infinity,
                        width: double.infinity,
                        fit: BoxFit.cover,
                      )
                    : _imageFromDB != null
                        ? Image.memory(
                            _imageFromDB,
                            height: double.infinity,
                            width: double.infinity,
                            fit: BoxFit.cover,
                          )
                        : Image.asset(
                            'assets/images/placeholder_image.jpg',
                            height: double.infinity,
                            width: double.infinity,
                            fit: BoxFit.cover,
                          ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
