import 'package:flutter/material.dart';
import 'package:meals_app/providers/ingredient_provider.dart';
import 'package:meals_app/providers/meal_provider.dart';
import 'package:meals_app/providers/step_provider.dart';
import 'package:provider/provider.dart';

class MealDetailScreen extends StatelessWidget {
  static const routeName = '/meal-detail';

  // final Meal meal;
  // MealDetailScreen({this.meal});

  @override
  Widget build(BuildContext context) {
    final altImage = 'assets/images/placeholder_image.jpg';
    final mealProvider = Provider.of<MealProvider>(context, listen: false);
    final ingredientProvider =
        Provider.of<IngredientProvider>(context, listen: false);
    final stepProvider = Provider.of<StepProvider>(context, listen: false);

    // final List<String> ingredientsList = [
    //   'Apfel',
    //   'Birne',
    // ];
    // final List<String> stepsList = [
    //   'Anfang',
    //   'Ende',
    //   'Fertig',
    // ];

    // Uint8List decodedBytes;

    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, Object>;
    final mealId = int.parse(routeArgs['id']);
    final mealTitle = routeArgs['title'];
    final decodedBytes = routeArgs['decodedBytes'];
    final duration = routeArgs['duration'];
    final complexity = routeArgs['complexity'];
    // final ingredients = routeArgs['ingredients'];
    // final steps = routeArgs['steps'];

    print('Übergebens meal:');
    print(mealId);
    print(mealTitle);
    print(decodedBytes);
    print(duration);
    print(complexity);
    // print(ingredients);
    // print(steps);

    // if (meal != null) {
    //   String title = meal.title;
    //   String imgUrl = meal.imageUrl;
    //   int duration = meal.duration;
    //   String complexity = meal.complexity;
    //   String category = meal.category;
    //   List<String> ingredients = meal.ingredients;
    //   List<String> steps = meal.steps;

    //   decodedBytes = base64Decode(meal.imageUrl);
    // }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          mealTitle,
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          // IconButton(
          //   icon: Icon(mealProvider.isFavorite(mealId) ? Icons.star : Icons.star_border),
          //   onPressed: () {
          //     mealProvider.changeIsFavorite(mealId),
          //   },
          // ),
        ],
      ),
      body:
          // StreamBuilder(
          // stream: FirebaseFirestore.instance
          //     .collection('meals')
          //     .doc(mealId)
          //     .snapshots(),
          // // stream: mealProvider.meals,
          // builder: (context, snapshot) {
          //   if (!snapshot.hasData) {
          //     return Center(
          //       child: CircularProgressIndicator(),
          //     );
          //   }
          //   return
          SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 250,
              width: double.infinity,
              child: decodedBytes != null
                  ? Image.memory(
                      decodedBytes,
                      fit: BoxFit.cover,
                    )
                  : Image.asset(
                      altImage,
                      fit: BoxFit.cover,
                    ),
            ),

            // Ingredients Headline
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Text(
                'Zutaten',
                style: Theme.of(context).textTheme.headline6,
              ),
            ),

            //Ingredients List
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 250,
              width: double.infinity,
              child: FutureBuilder(
                future: ingredientProvider.ingredients,
                builder: (context, snapshot) {
                  return snapshot.data.length > 0
                      ? ListView.builder(
                          itemBuilder: (ctx, index) {
                            if (snapshot.data.length < 1) {
                              return SizedBox();
                            } else if (!snapshot.data[index].meal
                                .toString()
                                .contains(mealId.toString())) {
                              return SizedBox();
                            } else {
                              return Card(
                                color: Theme.of(context).accentColor,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  child: Text(
                                    // snapshot.data['ingredients'][index],

                                    snapshot.data[index].name,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                              );
                            }
                          },
                          itemCount: snapshot.data.length,
                        )
                      : Center(
                          child: Container(
                            margin: EdgeInsets.all(30),
                            child: Text(
                                'Du hast dem Rezept noch keine Zutaten hinzugefügt'),
                          ),
                        );
                },
              ),
            ),

            //Steps Headline
            Container(
              child: Text(
                'Steps',
                style: Theme.of(context).textTheme.headline6,
              ),
            ),

            //Steps List
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.only(top: 10, bottom: 10),
              height: 250,
              width: double.infinity,
              child: FutureBuilder(
                future: stepProvider.steps,
                builder: (context, snapshot) {
                  return snapshot.data.length > 0
                      ? ListView.builder(
                          itemBuilder: (ctx, index) {
                            if (snapshot.data.length < 1) {
                              return SizedBox();
                            } else if (!snapshot.data[index].meal
                                .toString()
                                .contains(mealId.toString())) {
                              return SizedBox();
                            } else {
                              return Column(
                                children: [
                                  ListTile(
                                    leading: CircleAvatar(
                                      child: Text('# ${index + 1}'),
                                    ),
                                    title: Text(snapshot.data[index].name),
                                    // Text(snapshot.data['steps'][index]),
                                  ),
                                  // Trennlinie
                                  Divider(),
                                ],
                              );
                            }
                          },
                          itemCount: snapshot.data.length,
                        )
                      : Center(
                          child: Container(
                            margin: EdgeInsets.all(30),
                            child: Text(
                              'Du hast dem Rezept keine Arbeitsschritte hinzugefügt',
                              // textAlign: TextAlign.center,
                            ),
                          ),
                        );
                },
              ),
            ),
          ],
        ),
      ),
    );

    // floatingActionButton: FloatingActionButton(
    //   child: Icon(
    //     isFavorite(mealId) ? Icons.star : Icons.star_border,
    //   ),
    //   onPressed: () => {
    //     toggleFavorite(mealId),
    //   },
    // ),
  }
}
