import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:meals_app/providers/meal_provider.dart';
import 'package:provider/provider.dart';
import '../widgets/meal_item.dart';

class CategoryMealsScreen extends StatelessWidget {
  static const routeName = '/category-meals';

  @override
  Widget build(BuildContext context) {
    final mealList = Provider.of<MealProvider>(context).meals;

    final altImage = 'assets/images/placeholder_image.jpg';
    final altComplexitiy = '??? ;)';
    Uint8List decodedBytes;

    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final categoryTitle = routeArgs['title'];
    // final categoryId = routeArgs['id'];

    return Scaffold(
      appBar: AppBar(
        title: Text(
          categoryTitle,
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [],
      ),
      body: FutureBuilder(
        future: mealList,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.data.length != null) {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (ctx, index) {
                // if (snapshot.data[index].category != null) {
                //   print(snapshot.data[index].category);
                //   print(snapshot.data.length);
                // }
                if (snapshot.data[index].category == null) {
                  return SizedBox();
                } else if (!snapshot.data[index].category
                    .contains(categoryTitle)) {
                  return SizedBox();
                } else {
                  if (snapshot.data[index].imageUrl != null) {
                    decodedBytes = base64Decode(snapshot.data[index].imageUrl);
                    print('Bild vorhanden: $decodedBytes');
                  } else {
                    decodedBytes = null;
                    print('Bild null: $decodedBytes');
                  }
                  return MealItem(
                    id: snapshot.data[index].id,
                    title: snapshot.data[index].title,
                    decodedBytes: decodedBytes,
                    duration: snapshot.data[index].duration,
                    complexity:
                        snapshot.data[index].complexity ?? altComplexitiy,
                    isFavorite: snapshot.data[index].isFavorite,
                    category: snapshot.data[index].category,
                  );
                }
              },
            );
          } else {
            return SizedBox();
          }
        },
      ),
    );
  }
}
