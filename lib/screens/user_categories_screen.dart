import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:meals_app/providers/category_provider.dart';
import 'package:meals_app/screens/edit_categorie_screen.dart';
import 'package:meals_app/widgets/main_drawer.dart';
import 'package:provider/provider.dart';

class UserCategoriesScreen extends StatelessWidget {
  static const routeName = '/user-categories';

  Future<void> _deleteDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Wirklich löschen!"),
                ],
              ),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  // Schließt Keyboard
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.of(context).pop();
                },
                child: Text('Löschen'),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // final scaffold = Scaffold.of(context);
    final categoryList = Provider.of<CategoryProvider>(context).categories;
    Uint8List decodedBytes;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Meine Kategorien',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(EditCategoryScreen.routeName);
            },
          ),
        ],
      ),
      drawer: MainDrawer(),
      body: FutureBuilder(
          future: categoryList,
          builder: (context, snapshot) {
            if (snapshot.connectionState != ConnectionState.waiting) {
              return Padding(
                padding: EdgeInsets.all(8),
                child: ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      if (snapshot.data[index].imageUrl != null) {
                        decodedBytes =
                            base64Decode(snapshot.data[index].imageUrl);
                      }
                      return Column(
                        children: [
                          ListTile(
                            title: Text(snapshot.data[index].name),
                            leading: snapshot.data[index].imageUrl == null
                                ? CircleAvatar(
                                    backgroundImage: AssetImage(
                                        'assets/images/placeholder_image.jpg'),
                                  )
                                : CircleAvatar(
                                    backgroundImage:
                                        Image.memory(decodedBytes).image),
                            // CircleAvatar(
                            //     backgroundImage: AssetImage(
                            //         snapshot.data[index].imageUrl),
                            //   ),
                            trailing: Container(
                              width: 100,
                              child: Row(
                                children: [
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              EditCategoryScreen(
                                                  category:
                                                      snapshot.data[index]),
                                        ),
                                      );
                                    },
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.delete),
                                    onPressed: () async {
                                      await showDialog(
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            content: Form(
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Text('Wirklich löschen!'),
                                                ],
                                              ),
                                            ),
                                            actions: [
                                              TextButton(
                                                child: Text('Abbrechen'),
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                              ),
                                              TextButton(
                                                onPressed: () async {
                                                  try {
                                                    await Provider.of<
                                                                CategoryProvider>(
                                                            context,
                                                            listen: false)
                                                        .removeCategory(snapshot
                                                            .data[index].id);
                                                  } catch (error) {
                                                    ScaffoldMessenger.of(
                                                            context)
                                                        .showSnackBar(SnackBar(
                                                      backgroundColor:
                                                          Colors.red,
                                                      content: Text(
                                                        'Konnte nicht gelöscht werden',
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ));
                                                  }
                                                  Navigator.of(context).pop();
                                                },
                                                child: Text('Löschen'),
                                              )
                                            ],
                                          );
                                        },
                                      );
                                    },
                                    color: Theme.of(context).errorColor,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Divider(),
                        ],
                      );
                    }),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).accentColor,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).pushNamed(EditCategoryScreen.routeName);
        },
      ),
    );
  }
}
