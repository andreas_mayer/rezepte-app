import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:meals_app/providers/category_provider.dart';
import 'package:provider/provider.dart';
import '../widgets/category_item.dart';

class CategoriesScreen extends StatelessWidget {
  // final altImage = 'assets/images/placeholder_image.jpg';

  @override
  Widget build(BuildContext context) {
    final categoryList = Provider.of<CategoryProvider>(context).categories;
    Uint8List decodedBytes;
    print('DecodedBytes: $decodedBytes');

    return FutureBuilder(
        future: categoryList,
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.waiting) {
            return GridView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                if (snapshot.data[index].imageUrl != null) {
                  decodedBytes = base64Decode(snapshot.data[index].imageUrl);
                  print('Bild vorhanden: $decodedBytes');
                  print(snapshot.data[index].id);
                  print(snapshot.data[index].name);
                } else {
                  decodedBytes = null;
                  print('Bild null: $decodedBytes');
                  print(snapshot.data[index].id);
                  print(snapshot.data[index].name);
                }
                return CategoryItem(
                  snapshot.data[index].id,
                  snapshot.data[index].name,
                  // decodedBytes ?? altImage,
                  decodedBytes,
                );
              },

              padding: const EdgeInsets.all(25),

              // Sliver (Scrollbar), GridDelegate (kümmert sich um Breite und Höhe der einzelnen Elemente)
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                // Breite eines Eintrags (Kategorie)
                maxCrossAxisExtent: 200,
                // Verhältnis Breie zu Höhe
                childAspectRatio: 3 / 2,
                // Abstand zwischen den Elementen
                crossAxisSpacing: 20,
                // Abstand zwischen oben und unten
                mainAxisSpacing: 20,
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}
