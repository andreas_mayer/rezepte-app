import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meals_app/models/ingredient.dart';
import 'package:meals_app/models/meal.dart';
import 'package:meals_app/models/step.dart';
import 'package:meals_app/providers/category_provider.dart';
import 'package:meals_app/providers/ingredient_provider.dart';
import 'package:meals_app/providers/meal_provider.dart';
import 'package:meals_app/providers/step_provider.dart';
import 'package:provider/provider.dart';

class EditMealScreen extends StatefulWidget {
  static const routeName = 'edit-meal';

  final Meal meal;
  final Ingredient ingredient;
  final MyStep step;
  EditMealScreen({
    this.meal,
    this.ingredient,
    this.step,
  });

  @override
  _EditMealScreenState createState() => _EditMealScreenState();
}

class _EditMealScreenState extends State<EditMealScreen> {
  // Auswahl für Dropdown
  List<String> complexityValues = [
    "einfach",
    "herausfordernd",
    "schwer",
  ];

  Uint8List _decodedBytes;

  Color _iconColor = Colors.grey.shade300;

  // List<String> ingredients = [];
  // List<String> steps = [];

  // String imageUrl;

  bool isLoading = false;

  // Für RadioListTile
  String complexityValue;
  String categoryValue;

  File _imageFile;

  final _formKey1 = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  final _formKey3 = GlobalKey<FormState>();
  final _formKeyIngredients = GlobalKey<FormState>();
  final _formKeyStep = GlobalKey<FormState>();
  final _dialogIngredientKey = GlobalKey<FormState>();
  final _dialogStepKey = GlobalKey<FormState>();

  final titleController = TextEditingController();
  final imageUrlController = TextEditingController();
  final durationController = TextEditingController();
  final ingredientController = TextEditingController();
  final stepController = TextEditingController();
  final complexityController = TextEditingController();
  final categoryController = TextEditingController();
  final dialogIngredientController = TextEditingController();
  final dialogStepController = TextEditingController();
  final favoriteController = TextEditingController();

  @override
  void dispose() {
    titleController.dispose();
    imageUrlController.dispose();
    durationController.dispose();
    ingredientController.dispose();
    stepController.dispose();
    dialogIngredientController.dispose();
    dialogStepController.dispose();
    complexityController.dispose();
    categoryController.dispose();
    favoriteController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    final mealProvider = Provider.of<MealProvider>(context, listen: false);

    // final categoryProvider =
    //     Provider.of<CategoryProvider>(context, listen: false);

    if (widget.meal != null) {
      //Edit

      titleController.text = widget.meal.title;
      imageUrlController.text = widget.meal.imageUrl;
      durationController.text = widget.meal.duration.toString();
      complexityController.text = widget.meal.complexity;
      categoryController.text = widget.meal.category;
      favoriteController.text = widget.meal.isFavorite.toString();

      print('ImageControllerText:');
      print(imageUrlController.text);

      if (imageUrlController.text != null && imageUrlController.text != '') {
        _decodedBytes = base64Decode(imageUrlController.text);
        print('Controller nicht null');
      }

      // Load existing meal
      mealProvider.loadAll(widget.meal);
      if (mealProvider.isFavorite == 1) _iconColor = Colors.yellow.shade600;
    } else {
      // new meal
      mealProvider.loadAll(null);
    }

    super.initState();
  }

  // Popupdialog Zutaten bearbeiten
  Future<void> _showIngredientDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              key: _dialogIngredientKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextFormField(
                    controller: dialogIngredientController,
                    validator: (value) {
                      return value.isNotEmpty
                          ? null
                          : 'Du musst etwas eingeben';
                    },
                    decoration: InputDecoration(hintText: 'Zutat ändern'),
                    onFieldSubmitted: (value) {
                      dialogIngredientController.text = value;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                ],
              ),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  if (_dialogIngredientKey.currentState.validate()) {
                    // Schließt Keyboard
                    FocusScope.of(context).requestFocus(FocusNode());
                    Navigator.of(context).pop();
                  }
                },
                child: Text('Speichern'),
              ),
            ],
          );
        });
  }

  // Popupdialog Steps bearbeiten
  Future<void> _showStepDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Form(
              key: _dialogStepKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextFormField(
                    controller: dialogStepController,
                    validator: (value) {
                      return value.isNotEmpty
                          ? null
                          : 'Du musst etwas eingeben';
                    },
                    decoration: InputDecoration(hintText: 'Step ändern'),
                    onFieldSubmitted: (value) {
                      dialogStepController.text = value;
                    },
                  )
                ],
              ),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  if (_dialogStepKey.currentState.validate()) {
                    // Schließt Keyboard
                    FocusScope.of(context).requestFocus(FocusNode());
                    Navigator.of(context).pop();
                  }
                },
                child: Text('Speichern'),
              ),
            ],
          );
        });
  }

  final picker = ImagePicker();

  // Image von der Kamera
  Future pickImageFromCamera() async {
    final mealProvider = Provider.of<MealProvider>(context, listen: false);
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 20);

    final bytes = await pickedFile.readAsBytes();
    String img64 = base64Encode(bytes);

    setState(() {
      _decodedBytes = null;
      mealProvider.changeImageUrl = img64;
      _imageFile = File(pickedFile.path);
      imageUrlController.clear();
    });
  }

  // Image aus der Gallery
  Future pickImageFromGallery() async {
    final mealProvider = Provider.of<MealProvider>(context, listen: false);
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 30);

    final bytes = await pickedFile.readAsBytes();
    String img64 = base64Encode(bytes);

    setState(() {
      _decodedBytes = null;
      mealProvider.changeImageUrl = img64;
      _imageFile = File(pickedFile.path);
      imageUrlController.clear();
    });
  }

  // Bild aus Vorschau löschen
  void _clear() {
    final mealProvider = Provider.of<MealProvider>(context, listen: false);
    setState(() {
      _decodedBytes = null;
      mealProvider.changeImageUrl = null;
      imageUrlController.clear();
      _imageFile = null;
    });
  }

  // Zutat hinzufügen
  void _addIngredientToList() {
    final ingredientProvider =
        Provider.of<IngredientProvider>(context, listen: false);
    final isValid = _formKeyIngredients.currentState.validate();
    if (!isValid) {
      return;
    }
    _formKeyIngredients.currentState.save();
    setState(() {
      ingredientProvider.changeMeal = widget.meal.id;
      ingredientProvider.changeName = ingredientController.text;
      ingredientProvider.saveIngredient();
      // ingredients.add(ingredientController.text);
      ingredientController.clear();
    });
  }

  // Arbeitschritt zur Liste hinzufügen
  void _addStepToList() {
    final stepProvider = Provider.of<StepProvider>(context, listen: false);
    final isValid = _formKeyStep.currentState.validate();
    if (!isValid) {
      return;
    }
    _formKeyStep.currentState.save();
    setState(() {
      stepProvider.changeMeal = widget.meal.id;
      stepProvider.changeName = stepController.text;
      stepProvider.saveStep();
      // steps.add(stepController.text);
      stepController.clear();
    });
  }

  // Favoritenstatus ändern
  void _toggleFavorite() {
    final mealProvider = Provider.of<MealProvider>(context, listen: false);
    setState(() {
      if (mealProvider.isFavorite != 1) {
        mealProvider.changeIsFavorite = 1;
        _iconColor = Colors.yellow.shade600;
      } else if (mealProvider.isFavorite == 0) {
        mealProvider.changeIsFavorite = 1;
        _iconColor = Colors.yellow.shade600;
      } else {
        mealProvider.changeIsFavorite = 0;
        _iconColor = Colors.grey.shade300;
      }

      mealProvider.saveMeal();
    });
  }

  // Dropdown für Categorieauswahl
  // Category _selectedCategory;

  // callback(selectedCategory) {
  //   final mealProvider = Provider.of<MealProvider>(context, listen: false);
  //   setState(() {
  //     mealProvider.changeCategory = selectedCategory;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    final categoryList = Provider.of<CategoryProvider>(context).categories;
    final mealProvider = Provider.of<MealProvider>(context);
    final ingredientProvider = Provider.of<IngredientProvider>(context);
    final stepProvider = Provider.of<StepProvider>(context);

    // Startwert für Radiobutton
    String currentCategory = mealProvider.category;
    String currentComplexity = mealProvider.complexity;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Rezept bearbeiten',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () async {
                final isValid = (_formKey1.currentState.validate() &&
                    _formKey2.currentState.validate());
                if (!isValid) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: Colors.red,
                      content: const Text(
                        'Überprüfe bitte nochmal deine Eingabe',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  );
                  return;
                }
                _formKey1.currentState.save();
                _formKey2.currentState.save();

                // if (imageUrlController.text == '') {
                //   if (_imageFile != null) {
                //     print('Image vorhanden');
                //     await uploadImageToFirebase(context);
                //   }
                // }
                // print('SaveMeal');
                // mealProvider.changeIngredients = ingredients;
                // mealProvider.changeSteps = steps;
                mealProvider.changeTitle = titleController.text;
                mealProvider.changeDuration =
                    int.parse(durationController.text);
                // mealProvider.changeCategory = categoryController.text;
                // mealProvider.changeComplexity = complexityController.text;
                mealProvider.saveMeal();

                // ingredientProvider.saveIngredient();
                // stepProvider.saveStep();

                Navigator.of(context).pop();
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 100,
              width: double.infinity,
              child: Form(
                key: _formKey1,
                child: TextFormField(
                  decoration: InputDecoration(labelText: 'Name'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Bitte einen Namen für das Rezept eingeben';
                    }
                    return null;
                  },
                  onFieldSubmitted: (value) {
                    titleController.text = value;
                  },
                  controller: titleController,
                  textInputAction: TextInputAction.next,
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 100,
              width: double.infinity,
              child: Form(
                key: _formKey2,
                child: TextFormField(
                  decoration: InputDecoration(labelText: 'Dauer (Minuten)'),
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wie lange brauchst du ungefähr für das Gericht?';
                    }
                    return null;
                  },
                  onFieldSubmitted: (value) => durationController.text = value,
                  controller: durationController,
                  textInputAction: TextInputAction.next,
                ),
              ),
            ),
            // Container(
            //   decoration: BoxDecoration(
            //     color: Colors.white,
            //     border: Border.all(color: Colors.grey),
            //     borderRadius: BorderRadius.circular(10),
            //   ),
            //   margin: EdgeInsets.all(20),
            //   padding: EdgeInsets.all(10),
            //   height: 100,
            //   width: double.infinity,
            //   child: Form(
            //     key: _formKey3,
            //     child: TextFormField(
            //       decoration: InputDecoration(labelText: 'ImageUrl'),

            //       onChanged: (String value) =>
            //           mealProvider.changeImageUrl = value,
            //       controller: imageUrlController,
            //       textInputAction: TextInputAction.next,
            //     ),
            //   ),
            // ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FloatingActionButton(
                  heroTag: 'buttonCamera',
                  backgroundColor: Theme.of(context).accentColor,
                  child: Icon(Icons.add_a_photo),
                  onPressed: () => pickImageFromCamera(),
                ),
                SizedBox(
                  width: 30,
                ),
                FloatingActionButton(
                  heroTag: 'buttonAssets',
                  backgroundColor: Theme.of(context).accentColor,
                  child: Icon(Icons.photo_album),
                  onPressed: () => pickImageFromGallery(),
                ),
                SizedBox(
                  width: 30,
                ),
                FloatingActionButton(
                  heroTag: 'buttonDelete',
                  backgroundColor: Theme.of(context).accentColor,
                  child: Icon(Icons.delete),
                  onPressed: _clear,
                ),
              ],
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 200,
              width: double.infinity,
              child: Center(
                child: _imageFile != null
                    ? Image.file(
                        _imageFile,
                        height: double.infinity,
                        width: double.infinity,
                        fit: BoxFit.cover,
                      )
                    : _decodedBytes != null
                        ? Image.memory(
                            _decodedBytes,
                            height: double.infinity,
                            width: double.infinity,
                            fit: BoxFit.cover,
                          )
                        : Image.asset(
                            'assets/images/placeholder_image.jpg',
                            height: double.infinity,
                            width: double.infinity,
                            fit: BoxFit.cover,
                          ),

                //  _decodedBytes != null
                //     ? Image.memory(
                //         _decodedBytes,
                //         height: double.infinity,
                //         width: double.infinity,
                //         fit: BoxFit.cover,
                //       )
                //     : _imageFile != null
                //         ? Image.file(
                //             _imageFile,
                //             height: double.infinity,
                //             width: double.infinity,
                //             fit: BoxFit.cover,
                //           )
                //         : Image.asset(
                //             'assets/images/placeholder_image.jpg',
                //             height: double.infinity,
                //             width: double.infinity,
                //             fit: BoxFit.cover,
                //           ),
              ),
            ),

            // Favorit setzen
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 150,
              width: double.infinity,
              child: IconButton(
                  icon: Icon(Icons.star),
                  iconSize: 120,
                  color: _iconColor,
                  onPressed: () {
                    _toggleFavorite();
                  }),
            ),

            // Schwierigkeit auswählen
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 190,
              width: double.infinity,
              child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemCount: complexityValues.length,
                itemBuilder: (context, index) {
                  return RadioListTile(
                    title: Text(complexityValues[index]),
                    value: complexityValues[index],
                    groupValue: currentComplexity,
                    onChanged: (complexityValue) {
                      mealProvider.changeComplexity = complexityValue;

                      currentComplexity = complexityValue;
                    },
                  );
                },
              ),
            ),

            // Kategorie auswählen
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 190,
              width: double.infinity,
              child: FutureBuilder(
                future: categoryList,
                builder: (context, snapshot) {
                  if (snapshot.connectionState != ConnectionState.waiting) {
                    return snapshot.data.length > 0
                        ? ListView.builder(
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              return RadioListTile(
                                  title: Text(snapshot.data[index].name),
                                  value: snapshot.data[index].name,
                                  groupValue: currentCategory,
                                  onChanged: (categoryValue) {
                                    mealProvider.changeCategory = categoryValue;

                                    currentCategory = categoryValue;
                                  });
                            },
                          )
                        : Center(
                            child: Text('Du hast noch keine Kategorien'),
                          );
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),

            // Zutaten hinzufügen
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 100,
              width: double.infinity,
              child: Form(
                key: _formKeyIngredients,
                child: TextFormField(
                  decoration: InputDecoration(labelText: 'Zutat hinzufügen'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Die Zutat braucht einen Namen';
                    }
                    return null;
                  },

                  // onFieldSubmitted: (value) {
                  //   ingredientProvider.changeName = value;
                  //   ingredientProvider.saveIngredient();

                  //   ingredientController.clear();
                  // },

                  // onChanged: (String value) =>
                  //     mealProvider.changeIngredient = value,
                  controller: ingredientController,
                  textInputAction: TextInputAction.done,
                ),
              ),
            ),

            FloatingActionButton(
              heroTag: 'addIngredient',
              backgroundColor: Theme.of(context).accentColor,
              child: Icon(Icons.add),
              onPressed: _addIngredientToList,
            ),

            // Zutaten anzeigen und bearbeiten
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.only(bottom: 10, top: 10),
              height: 190,
              width: double.infinity,
              child: FutureBuilder(
                future: ingredientProvider.ingredients,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return snapshot.data.length > 0
                      ? ListView.builder(
                          // shrinkWrap: true,
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            if (snapshot.data[index].meal == widget.meal.id) {
                              final item = snapshot.data[index].name;
                              return Dismissible(
                                key: Key(item),
                                onDismissed: (direction) {
                                  setState(() {
                                    ingredientProvider.removeIngredient(
                                        snapshot.data[index].id);
                                  });
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      backgroundColor: Colors.red,
                                      content: Text(
                                        '$item wurde gelöscht',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 20),
                                      ),
                                    ),
                                  );
                                },
                                background: Container(
                                  color: Colors.red,
                                ),
                                child: ListTile(
                                  title: Text('$item'),
                                  trailing: Container(
                                    width: 50,
                                    // width: 100,
                                    child: Row(
                                      children: [
                                        // IconButton(
                                        //   icon: Icon(Icons.edit),
                                        //   onPressed: () async {
                                        //     await _showIngredientDialog(
                                        //         context);
                                        //     setState(() {
                                        //       ingredientProvider
                                        //           .removeIngredient(
                                        //               snapshot.data[index].id);
                                        //       ingredientProvider.changeName =
                                        //           dialogIngredientController
                                        //               .text;
                                        //       ingredientProvider
                                        //           .saveIngredient();
                                        //     });
                                        //   },
                                        //   color: Theme.of(context).primaryColor,
                                        // ),
                                        IconButton(
                                          icon: Icon(Icons.delete),
                                          onPressed: () {
                                            setState(() {
                                              ingredientProvider
                                                  .removeIngredient(
                                                      snapshot.data[index].id);
                                            });
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(
                                              SnackBar(
                                                backgroundColor: Colors.red,
                                                content: Text(
                                                  '$item wurde gelöscht',
                                                  style:
                                                      TextStyle(fontSize: 20),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            );
                                          },
                                          color: Theme.of(context).errorColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            } else {
                              return SizedBox();
                            }
                          },
                        )
                      : Center(
                          child: Text('Du hast noch keine Zutaten hinzugefügt'),
                        );
                },
              ),
            ),

            // Arbeitsschritt hinzufügen
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(20),
              padding: EdgeInsets.all(10),
              height: 100,
              width: double.infinity,
              child: Form(
                key: _formKeyStep,
                child: TextFormField(
                  decoration:
                      InputDecoration(labelText: 'Arbeitsschritt hinzufügen'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'So wird das Essen nie fertig';
                    }
                    return null;
                  },

                  // onSubmitted: (value) {
                  //   steps.add(value);
                  //   ingredientController.clear();
                  // },

                  // onChanged: (String value) =>
                  //     mealProvider.changeSteps = value,
                  controller: stepController,
                  textInputAction: TextInputAction.done,
                ),
              ),
            ),

            FloatingActionButton(
              heroTag: 'addStep',
              backgroundColor: Theme.of(context).accentColor,
              child: Icon(Icons.add),
              onPressed: _addStepToList,
            ),

            // Arbeitsschritte anzeigen und bearbeiten
            Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10),
                ),
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.only(bottom: 10, top: 10),
                height: 190,
                width: double.infinity,
                child: FutureBuilder(
                  future: stepProvider.steps,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return snapshot.data.length > 0
                        ? ListView.builder(
                            // shrinkWrap: true,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              if (snapshot.data[index].meal == widget.meal.id) {
                                final item = snapshot.data[index].name;
                                return Dismissible(
                                  key: Key(item),
                                  onDismissed: (direction) {
                                    setState(() {
                                      stepProvider
                                          .removeStep(snapshot.data[index].id);
                                    });
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        backgroundColor: Colors.red,
                                        content: Text(
                                          'Arbeitsschritt wurde gelöscht',
                                          style: TextStyle(fontSize: 20),
                                        ),
                                      ),
                                    );
                                  },
                                  background: Container(
                                    color: Colors.red,
                                  ),
                                  child: ListTile(
                                    title: Text('$item'),
                                    trailing: Container(
                                      width: 50,
                                      // width: 100,
                                      child: Row(
                                        children: [
                                          // IconButton(
                                          //   icon: Icon(Icons.edit),
                                          //   onPressed: () async {
                                          //     await _showStepDialog(context);
                                          //     setState(() {
                                          //       stepProvider.removeStep(
                                          //           snapshot.data[index].id);
                                          //       stepProvider.changeName =
                                          //           dialogStepController.text;
                                          //       stepProvider.saveStep();
                                          //     });
                                          //   },
                                          //   color:
                                          //       Theme.of(context).primaryColor,
                                          // ),
                                          IconButton(
                                            icon: Icon(Icons.delete),
                                            onPressed: () {
                                              setState(() {
                                                stepProvider.removeStep(
                                                    snapshot.data[index].id);
                                              });
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(
                                                SnackBar(
                                                  backgroundColor: Colors.red,
                                                  content: Text(
                                                    'Arbeitsschritt wurde gelöscht',
                                                    style:
                                                        TextStyle(fontSize: 20),
                                                  ),
                                                ),
                                              );
                                            },
                                            color: Theme.of(context).errorColor,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              } else {
                                return SizedBox();
                              }
                            },
                          )
                        : Center(
                            child: Text(
                                'Du hast noch keinen Arbeitsschritt hinzugefügt'),
                          );
                  },
                )),
          ],
        ),
      ),
    );
  }
}
