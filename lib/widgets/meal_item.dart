import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:meals_app/providers/meal_provider.dart';
import 'package:provider/provider.dart';
import '../screens/meal_detail_screen.dart';

class MealItem extends StatefulWidget {
  final int id;
  final String title;
  final int duration;
  final String complexity;
  final int isFavorite;
  final Uint8List decodedBytes;
  final String category;
  // final List ingredients;
  // final List steps;

  MealItem({
    this.id,
    this.title,
    this.decodedBytes,
    this.duration,
    this.complexity,
    this.isFavorite,
    this.category,
    // this.ingredients,
    // this.steps,
  });

  @override
  _MealItemState createState() => _MealItemState();
}

class _MealItemState extends State<MealItem> {
  Color _iconColor = Colors.grey.shade300;

  String get complexityText {
    switch (widget.complexity) {
      case 'einfach':
        return 'Simple';
        break;
      case 'herausfordernd':
        return 'Challenging';
        break;
      case 'schwer':
        return 'Hard';
        break;
      case 'finde es heraus':
        return 'Unknown';
        break;
      default:
        return 'Unknown';
    }
  }

  void selectMeal(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      MealDetailScreen.routeName,
      arguments: {
        'id': widget.id.toString(),
        'title': widget.title,
        'decodedBytes': widget.decodedBytes,
        'duration': widget.duration,
        'complexity': widget.complexity,
        // 'ingredients': ingredients,
        // 'steps': steps,
      },
    );
  }

  // Favoritenstatus ändern
  // void _toggleFavorite() {
  //   final mealProvider = Provider.of<MealProvider>(context, listen: false);
  //   setState(() {
  //     if (mealProvider.isFavorite != 1) {
  //       mealProvider.changeIsFavorite = 1;
  //       _iconColor = Colors.yellow.shade600;
  //     } else if (mealProvider.isFavorite == 0) {
  //       mealProvider.changeIsFavorite = 1;
  //       _iconColor = Colors.yellow.shade600;
  //     } else {
  //       mealProvider.changeIsFavorite = 0;
  //       _iconColor = Colors.grey.shade300;
  //     }

  //     mealProvider.saveMeal();
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    final altImage = 'assets/images/placeholder_image.jpg';
    final mealProvider = Provider.of<MealProvider>(context);

    return InkWell(
      onTap: () => selectMeal(context),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 4,
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                  child: widget.decodedBytes != null
                      ? Image.memory(
                          widget.decodedBytes,
                          height: 200,
                          width: double.infinity,
                          fit: BoxFit.cover,
                        )
                      : Image.asset(
                          altImage,
                          height: 200,
                          width: double.infinity,
                          fit: BoxFit.cover,
                        ),
                ),
                Positioned(
                  bottom: 20,
                  right: 10,
                  child: Container(
                    width: 300,
                    color: Colors.black54,
                    padding: EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 20,
                    ),
                    child: Text(
                      widget.title,
                      style: TextStyle(fontSize: 26, color: Colors.white),
                      softWrap: true,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.schedule,
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Text('${widget.duration} min'),
                    ],
                  ),
                  Row(
                    children: [
                      if (widget.complexity.contains('einfach'))
                        Icon(
                          Icons.sentiment_satisfied_alt,
                          color: Colors.green,
                        ),
                      if (widget.complexity.contains('herausfordernd'))
                        Icon(
                          Icons.sentiment_neutral,
                          color: Colors.orange,
                        ),
                      if (widget.complexity.contains('schwer'))
                        Icon(
                          Icons.sentiment_very_dissatisfied,
                          color: Colors.red,
                        ),
                      SizedBox(
                        width: 6,
                      ),
                      Text('${widget.complexity}'),
                    ],
                  ),
                  IconButton(
                    icon: Icon(Icons.star),
                    color: widget.isFavorite == 1
                        ? Colors.yellow.shade600
                        : Colors.grey.shade300,
                    onPressed: () {},
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
