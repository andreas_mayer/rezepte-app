import 'package:flutter/material.dart';
import 'package:meals_app/screens/user_categories_screen.dart';
import 'package:meals_app/screens/user_meals_screen.dart';
import '../screens/filters_screen.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          AppBar(
            title: Text(
              'Einstellungen',
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontWeight: FontWeight.w700,
                fontSize: 30,
              ),
            ),
          ),
          // Container(
          //   height: 100,
          //   width: double.infinity,
          //   padding: EdgeInsets.all(20),
          //   alignment: Alignment.centerLeft,
          //   color: Theme.of(context).accentColor,
          //   child: Text(
          //     'Einstellungen',
          //     style: TextStyle(
          //       color: Theme.of(context).primaryColor,
          //       fontWeight: FontWeight.w900,
          //       fontSize: 30,
          //     ),
          //   ),
          // ),
          SizedBox(
            height: 20,
          ),
          ListTile(
            leading: Icon(
              Icons.restaurant,
              size: 26,
            ),
            title: Text(
              'Übersicht',
              style: TextStyle(
                fontFamily: 'RobotoCondensed',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.of(context).pushReplacementNamed('/');
            },
          ),
          ListTile(
            leading: Icon(
              Icons.settings,
              size: 26,
            ),
            title: Text(
              'Filter',
              style: TextStyle(
                fontFamily: 'RobotoCondensed',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.of(context).pushNamed(FiltersScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.category,
              size: 26,
            ),
            title: Text(
              'Kategorien verwalten',
              style: TextStyle(
                fontFamily: 'RobotoCondensed',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.of(context).pushNamed(UserCategoriesScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.restaurant_menu,
              size: 26,
            ),
            title: Text(
              'Rezepte verwalten',
              style: TextStyle(
                fontFamily: 'RobotoCondensed',
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.of(context).pushNamed(UserMealsScreen.routeName);
            },
          ),
        ],
      ),
    );
  }
}
