import 'package:flutter/material.dart';
import 'package:meals_app/models/category.dart';

class CategoriesDropdown extends StatefulWidget {
  List<Category> categories;

  Function(Category) callback;

  CategoriesDropdown(
    this.categories,
    this.callback,
  );

  @override
  _CategoriesDropdownState createState() => _CategoriesDropdownState();
}

class _CategoriesDropdownState extends State<CategoriesDropdown> {
  @override
  Widget build(BuildContext context) {
    return DropdownButton<Category>(
      hint: Text(
        ('Wähle eine Kategory'),
      ),
      onChanged: (Category value) {
        setState(() {
          widget.callback(value);
        });
      },
      items: widget.categories.map((category) {
        return DropdownMenuItem(
          value: category,
          child: Text(category.name),
        );
      }).toList(),
    );
  }
}
