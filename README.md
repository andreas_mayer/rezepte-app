# Rezepte App

- Kategorien (Vorspeise, Hautgericht, Dessert, ...) können angelegt, editiert und gelöscht werden
- Rezepte können angelegt, editiert und gelöscht werden
- Rezepte können nach Kategorien angezeigt werden 
- Rezepte können als Favorit markiert werden

Ein Rezept kann mit folgenden Daten angelegt werden:

- Rezeptname
- Image
- Zubereitungsdauer
- Schwierigkeitsgrad
- Zutaten
- Arbeitsschritte

